package com.cwy.post_friend.frame.controller;

import com.cwy.post_friend.frame.annotation.reponse.Response;
import com.cwy.post_friend.frame.annotation.request.RequestBody;
import com.cwy.post_friend.frame.annotation.request.RequestMapping;
import com.cwy.post_friend.frame.annotation.request.RequestParam;
import com.cwy.post_friend.frame.bean.ControllerChain;
import com.cwy.post_friend.frame.bean.Handler;
import com.cwy.post_friend.frame.enum_.RequestMode;
import com.cwy.post_friend.frame.factory.RequestMap;
import com.cwy.post_friend.frame.tool.StringToJSONObject;
import com.cwy.post_friend.frame.view.InternalResourceViewResolver;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletInputStream;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

/**
 * @Classname DispatcherServlet0
 * @Description TODO
 * @Author stomach medicine
 * @Version 1.0.0
 * @Create 2023-12-31 19:49
 * @Since 1.0.0
 */

public class DispatcherServlet0 extends HttpServlet {
    private final Map<String, Object> controllerChainMap = new HashMap<>();
    private final Map<String, Handler> handlerMap = new HashMap<>();
    private InternalResourceViewResolver viewResolver;
    private final Logger logger = Logger.getLogger("com.cwy.post_friend.frame.controller.DispatcherServlet0");

    @Override
    public void init() throws ServletException {
        String prefix = getInitParameter("prefix");
        String suffix = getInitParameter("suffix");
        viewResolver = new InternalResourceViewResolver(prefix, suffix);
        registerControllerChain();
        super.init();
    }

    public void registerControllerChain() {
        RequestMap requestMap = RequestMap.newInstance();
        Map<String, Object> requestMapping = requestMap.getRequestMapping();
        Set<Map.Entry<String, Object>> requestMappingEntrySet = requestMapping.entrySet();
        // 保存 Controller 类上注解的 URL 映射与 ControllerChain
        for (Map.Entry<String, Object> entry : requestMappingEntrySet) {
            controllerChainMap.put(entry.getKey(), entry.getValue());
            // 循环保存 ControllerChain 容器的 URL 映射与 ControllerChain
            controllerChainMap.forEach(((s, o) -> {
                ControllerChain controllerChain = (ControllerChain) o;
                Method[] clazzDeclaredMethods = controllerChain.getController().getClass().getDeclaredMethods();
                // 循环 Controller 的函数
                for (Method method : clazzDeclaredMethods) {
                    RequestMapping declaredAnnotation = method.getDeclaredAnnotation(RequestMapping.class);
                    if (declaredAnnotation != null) {
                        String value = declaredAnnotation.value();
                        RequestMode mode = declaredAnnotation.mode();
                        Parameter[] parameters = method.getParameters();
                        Map<Object, Integer> parameterMap = new HashMap<>();
                        for (int i = 0; i < parameters.length; i++) {
                            RequestBody requestBody = parameters[i].getDeclaredAnnotation(RequestBody.class);
                            RequestParam requestParam = parameters[i].getDeclaredAnnotation(RequestParam.class);
                            if (parameters[i].getType().equals(HttpServletRequest.class) ||
                                    parameters[i].getType().equals(HttpServletResponse.class) ||
                                    parameters[i].getType().equals(HttpSession.class)) {
                                parameterMap.put(parameters[i].getType(), i);
                            }
                            if (requestBody != null) {
                                parameterMap.put(new Object[]{parameters[i].getType()}, i);
                            }
                            if (requestParam != null) {
                                parameterMap.put(new String[]{requestParam.value()}, i);
                            }
                        }
                        Handler handler = new Handler(controllerChain.getController(), method, mode, parameterMap);
                        // 整合
                        handlerMap.put(entry.getKey() + value, handler);
                        logger.info(handlerMap.toString());
                    }
                }
            }));
        }
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        boolean hasResource = false;
        PrintWriter writer = resp.getWriter();
        ServletInputStream inputStream = req.getInputStream();
        InputStreamReader isr = new InputStreamReader(inputStream);
        StringBuilder sb = new StringBuilder();
        int i;
        while ((i = isr.read()) != -1) {
            sb.append((char) i);
        }
        Set<Map.Entry<String, Handler>> entries = handlerMap.entrySet();
        // 循环遍历 HandlerMap，对应每个 Handler 所对应的 URL，做相应的工作
        Object invoke;
        for (Map.Entry<String, Handler> entry : entries) {
            if (req.getRequestURI().equals(entry.getKey())) {
                hasResource = true;
                Handler handler = handlerMap.get(entry.getKey());
                Method method = handler.getMethod();
                RequestMode requestMode = handler.getRequestMode();
                String requestMethod = req.getMethod();
                if (!requestMode.name().equals(requestMethod)) {
                    resp.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                    return;
                }
                Response response = method.getDeclaredAnnotation(Response.class);
                try {
                    Map<Object, Integer> paramClassNameMap0 = handler.getParamClassNameMap0();
                    Object[] objects = new Object[paramClassNameMap0.size()];
                    // 遍历函数参数，给函数参数赋值
                    paramClassNameMap0.forEach((o, i0) -> {
                        if (o.equals(HttpServletRequest.class)) {
                            objects[i0] = req;
                        } else if (o.equals(HttpServletResponse.class)) {
                            objects[i0] = resp;
                        } else if (o.equals(HttpSession.class)) {
                            objects[i0] = req.getSession();
                        } else if (o.getClass().equals(Object[].class)) {
                            Object[] o0 = (Object[]) o;
                            try {
                                objects[i0] = StringToJSONObject.stringToObject(sb.toString(), (Class<?>) o0[0]);
                            } catch (InstantiationException | IllegalAccessException e) {
                                throw new RuntimeException(e);
                            }
                        } else if (o.getClass().equals(String[].class)) {
                            String[] o1 = (String[]) o;
                            objects[i0] = req.getParameter(o1[0]);
                        }
                    });
                    invoke = handler.invoke(objects);
                    if (response != null) {
                        writer.println(invoke);
                    } else {
                        viewResolver.render((String) invoke, req, resp);
                    }
                } catch (InvocationTargetException | IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        if (!hasResource) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }
}
