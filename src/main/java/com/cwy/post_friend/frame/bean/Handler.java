package com.cwy.post_friend.frame.bean;

import com.cwy.post_friend.frame.enum_.RequestMode;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

/**
 * c
 * @projectName: post_friend
 * @package: com.cwy.post_friend.frame.bean
 * @className: Handler
 * @author: LGJ
 * @description: TODO
 * @date: 2023/12/24 16:25
 * @version: 1.0
 */
public class Handler {
    // 目标对象，执行函数所对应的目标对象，例如：method.invoke(object)，object 就是目标对象
    private Object target;
    // 目标方法，执行目标对象所对应的目标方法，例如：method.invoke(object)，method 就是目标对象
    private Method method;
    // 请求方式，例如：GET，POST，PUT，DELETE
    private RequestMode requestMode;
    // ****
    private Map<String,Integer> paramClassNameMap;
    // 函数参数类与参数位置，例如：public void hello(int i, String a)，[{int: 0}, {String: 1}]
    private Map<Object, Integer> paramClassNameMap0;

    public Handler(Object target, Method method,RequestMode requestMode) {
        this.target = target;
        this.method = method;
        this.requestMode = requestMode;
        this.paramClassNameMap = new HashMap<>();
    }

    public Handler(Object target, Method method, RequestMode requestMode, Map<Object, Integer> paramClassNameMap0) {
        this.target = target;
        this.method = method;
        this.requestMode = requestMode;
        this.paramClassNameMap0 = paramClassNameMap0;
    }

    public RequestMode getRequestMode() {
        return requestMode;
    }

    public void setRequestMode(RequestMode requestMode) {
        this.requestMode = requestMode;
    }

    public Object invoke(Object[] param) throws InvocationTargetException, IllegalAccessException {
        return method.invoke(target, param);
    }


    public Object getTarget() {
        return target;
    }

    public void setTarget(Object target) {
        this.target = target;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public Map<String, Integer> getParamClassNameMap() {
        return paramClassNameMap;
    }

    public void setParamClassNameMap(Map<String, Integer> paramClassNameMap) {
        this.paramClassNameMap = paramClassNameMap;
    }

    public Map<Object, Integer> getParamClassNameMap0() {
        return paramClassNameMap0;
    }

    public void setParamClassNameMap0(Map<Object, Integer> paramClassNameMap0) {
        this.paramClassNameMap0 = paramClassNameMap0;
    }

    @Override
    public String toString() {
        return "Handler{" +
                "target=" + target +
                ", method=" + method +
                ", requestMode=" + requestMode +
                ", paramClassNameMap=" + paramClassNameMap +
                ", paramClassNameMap0=" + paramClassNameMap0 +
                '}';
    }
}
