package com.cwy.post_friend.frame.tool;

import com.alibaba.druid.pool.DruidDataSourceFactory;
import com.cwy.post_friend.frame.exception.RollbackTransactionException;
import com.cwy.post_friend.frame.exception.SubmitTransactionException;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Logger;

/**
 * @Classname JDBCUtil
 * @Description TODO
 * @Author stomach medicine
 * @Version 1.0.0
 * @Create 2023-12-26 15:20
 * @Since 1.0.0
 */

public class JDBCUtil {
    private static final ThreadLocal<Connection> threadLocalConnections =
            new ThreadLocal<>();
    private static final DataSource dataSource;
    private static final Logger logger = Logger.getLogger("com.cwy.post_friend.frame.tool.JDBCUtil");

    static {
        try {
            Properties properties = new Properties();
            properties.load(Thread.currentThread().
                    getContextClassLoader().getResourceAsStream("druid/druid.properties"));
            ResourceBundle resourceBundle = ResourceBundle.getBundle("druid.druid");
            dataSource = DruidDataSourceFactory.createDataSource(properties);
            Class.forName(resourceBundle.getString("druid.driverClassName"));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private JDBCUtil() {
    }

    public static Connection getConnection() throws SQLException {
        Connection connection = dataSource.getConnection();
        threadLocalConnections.set(connection);
        return connection;
    }

    public static Connection getConnection(String url, String username, String password) throws SQLException {
        Connection connection = DriverManager.getConnection(url, username, password);
        threadLocalConnections.set(connection);
        return connection;
    }

    public static void openTransaction() {
        try {
            Connection connection = threadLocalConnections.get();
            if (connection == null) {
                connection = getConnection();
            }
            threadLocalConnections.set(connection);
            logger.info("开启事务成功");
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            logger.info("开启事务失败");
            throw new RuntimeException(e);
        }
    }

    public static void submitTransaction() {
        try {
            Connection connection = threadLocalConnections.get();
            if (connection == null) {
                throw new SubmitTransactionException("事务提交错误");
            }
            connection.commit();
            logger.info("提交事务成功");
            connection.setAutoCommit(true);
        } catch (SQLException | SubmitTransactionException e) {
            logger.info("提交事务失败");
            throw new RuntimeException(e);
        }
    }

    public static void rollbackTransaction() {
        try {
            Connection connection = threadLocalConnections.get();
            if (connection == null) {
                throw new RollbackTransactionException("事务回滚错误");
            }
            connection.rollback();
            connection.setAutoCommit(true);
            logger.info("事务回滚成功");
        } catch (RollbackTransactionException | SQLException e) {
            logger.info("事务回滚失败");
            throw new RuntimeException(e);
        }
    }
}
