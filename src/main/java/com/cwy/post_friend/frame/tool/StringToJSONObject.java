package com.cwy.post_friend.frame.tool;

import java.lang.reflect.Field;

/**
 * 字符串转 JSON 对象
 *
 * @Classname StringToJSONObject
 * @Description TODO
 * @Author stomach medicine
 * @Version 1.0.0
 * @Create 2023-12-31 21:46
 * @Since 1.0.0
 */

public class StringToJSONObject {
    private StringToJSONObject() {
    }

    /**
     * 需要将字符串转为指定类型的对象
     *
     * @param json  对应的字符串
     * @param clazz 对应的类型
     * @return 对应类型的对象
     */
    public static Object stringToObject(String json, Class<?> clazz) throws InstantiationException, IllegalAccessException {
        Object o = clazz.newInstance();
        json = json.replace("{", "").replace("}", "");
        String[] split = json.split(",");
        Field[] declaredFields = clazz.getDeclaredFields();
        for (Field declaredField : declaredFields) {
            declaredField.setAccessible(true);
            for (String s : split) {
                s = s.replace(" ","").replace("\r", "").replace("\n", "").replace("\"", "");
                int i1 = s.indexOf(":");
                String name = s.substring(0, i1);
                String value = s.substring(s.indexOf(":") + 1);
                if (name.equals(declaredField.getName())) {
                    if (declaredField.getType().equals(Integer.class)) {
                        declaredField.set(o, Integer.parseInt(value));
                    } else if (declaredField.getType().equals(Double.class)) {
                        declaredField.set(o, Double.valueOf(value));
                    }else if (declaredField.getType().equals(Float.class)) {
                        declaredField.set(o, Float.valueOf(value));
                    }else if (declaredField.getType().equals(String.class)) {
                        declaredField.set(o, value);
                    }
                }
            }
        }
        return o;
    }
}
