package com.cwy;


import io.minio.GetPresignedObjectUrlArgs;
import io.minio.MinioClient;
import io.minio.errors.*;
import io.minio.http.Method;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeUnit;

public class Test1 {
    /**
     * 测试 MinIO 的 API
     */
    @Test
    public void test1_0() throws ServerException, InsufficientDataException,
            ErrorResponseException, IOException, NoSuchAlgorithmException,
            InvalidKeyException, InvalidResponseException, XmlParserException,
            InternalException {
        MinioClient minioClient = MinioClient.
                builder().
                endpoint("http://127.0.0.1:9000").
                credentials("nahoZTXcYNqvZiXe2ttC", "M7iIwRudkf9QdxJv9LhcxO13p1MpjpIeRjprSpkk").
                build();
        String url = minioClient.getPresignedObjectUrl(
                GetPresignedObjectUrlArgs
                        .builder()
                        .method(Method.GET)
                        .bucket("bucker")
                        .object("quickSort.c")
                        .expiry(80, TimeUnit.SECONDS)
                        .build());
        System.out.println("url = " + url);
    }
}
